comand_selector () {

    if [ -z $command_selected ]; then
                
        read -p "  » " command_selected

    fi

    case $command_selected in

        start|st )

            /ant/42007/csgoserver start
            ;;

        stop|sp )

            /ant/42007/csgoserver stop
            ;;

        restart|r )

            /ant/42007/csgoserver restart
            ;;

        details|dt )

            /ant/42007/csgoserver details
            ;;

        update|u )

            /ant/42007/csgoserver details
            ;;

        update-lgsm|ul )

            /ant/42007/csgoserver details
            ;;

        backup|b )

            /ant/42007/csgoserver details
            ;;
                        
        console|c )

            /ant/42007/csgoserver details
            ;;

        debug|d )

            /ant/42007/csgoserver details
            ;;

        * )

            echo " Invalid command, please try again. "
            command_selected=""
            comand_selector

    esac

}

comand_selector