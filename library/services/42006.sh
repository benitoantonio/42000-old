comand_selector () {

    if [ -z $command_selected ]; then
            
            read -p "  » " command_selected

    fi

    case $command_selected in

            start|st )

                    /ant/42006/mcserver start
                    ;;

            stop|sp )

                    /ant/42006/mcserver stop
                    ;;

            restart|r )

                    /ant/42006/mcserver restart
                    ;;

            details|dt )

                    /ant/42006/mcserver details
                    ;;

            update|u )

                    /ant/42006/mcserver details
                    ;;

            update-lgsm|ul )

                    /ant/42006/mcserver details
                    ;;

            backup|b )

                    /ant/42006/mcserver details
                    ;;
                    
            console|c )

                    /ant/42006/mcserver details
                    ;;

            debug|d )

                    /ant/42006/mcserver details
                    ;;

            * )

                    echo " Invalid command, please try again. "
                    command_selected=""
                    comand_selector

    esac

}

comand_selector